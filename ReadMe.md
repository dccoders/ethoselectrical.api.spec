# EthosElectrical.Api.Spec

EthosElectrical.Api.Spec is the specification for the Field Application API.

## Installation

Import the specification into any OpenAPI 3.0 compatible service, to include [AWS APIGateway](https://aws.amazon.com/api-gateway/) or [Azure API Management](https://azure.microsoft.com/en-us/pricing/details/api-management/)

## Current Status

This specification is currently in active development.  The purpose of this repository is to make the specification publicly availible.  The current API (v1) is undergoing a full overhaul, of which the product will be stored here.

## Contributing
Public contributions are currently not accepted

## License
[MIT](https://choosealicense.com/licenses/mit/)